﻿CREATE TABLE [dbo].[News]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [Title] NVARCHAR(50) NOT NULL, 
    [Content] NVARCHAR(MAX) NOT NULL, 
    [Feed] INT NOT NULL, 
    CONSTRAINT [FK_News_Feed] FOREIGN KEY ([Feed]) REFERENCES [Feeds]([Id])
)
