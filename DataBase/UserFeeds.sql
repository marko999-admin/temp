﻿CREATE TABLE [dbo].[UserFeeds]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [UserId] INT NOT NULL, 
    [FeedId] INT NOT NULL, 
    CONSTRAINT [FK_UserId_Users] FOREIGN KEY (UserId) REFERENCES [Users]([Id]),
	CONSTRAINT [FK_FeedId_Feeds] FOREIGN KEY (FeedId) REFERENCES [Feeds]([Id])
)
