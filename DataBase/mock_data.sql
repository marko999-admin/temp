﻿--feeds table
SET IDENTITY_INSERT dbo.Feeds ON;
INSERT INTO dbo.Feeds (Id, Title) VALUES (1, 'Politics')
INSERT INTO dbo.Feeds (Id, Title) VALUES (2, 'Sport')
INSERT INTO dbo.Feeds (Id, Title) VALUES (3, 'LatestNews')
SET IDENTITY_INSERT dbo.Feeds OFF;
--news table
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Politics', 'Politics', 1)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Politics', 'Politics', 1)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Politics', 'Politics', 1)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Sport', 'Sport', 2)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Sport', 'Sport', 2)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('Sport', 'Sport', 2)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('LatestNews', 'LatestNews', 3)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('LatestNews', 'LatestNews', 3)
INSERT INTO dbo.News (Title, Content, Feed) VALUES ('LatestNews', 'LatestNews', 3)

--users table
SET IDENTITY_INSERT dbo.Users ON;
INSERT INTO dbo.Users(Id, Username, Password) VALUES (1, 'marko', 'pass')
INSERT INTO dbo.Users(Id, Username, Password) VALUES (2, 'test', 'test')
SET IDENTITY_INSERT dbo.Users OFF;

--user feeds table
INSERT INTO dbo.UserFeeds(FeedId, UserId) VALUES (1, 1)
INSERT INTO dbo.UserFeeds(FeedId, UserId) VALUES (2, 1)
INSERT INTO dbo.UserFeeds(FeedId, UserId) VALUES (1, 2)



