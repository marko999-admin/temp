## how to start project

#### Backend

Just  hit F5, database and everything is there, API will be running on fixed port
If database is missing, run build and publish on the project, that should create all tables also it should run mock_data.sql for populating tables with mock data

#### Frontend

On a command line navigate to project_root/Web/news-front
run:

(if you don't have angular-cli already installed)
npm install -g @angular/cli
npm i
ng serve --open

that should start angular app on port 4200 that will communicate with backend api
cors is setup, everything should work

## todo list(or what I haven't had time to fix or improve)

- search sql query: potential sql injection, use dapper parameters and commands
- authentication/authorization: at least two users, one admin that can add news, other one subscriber
- not fully happy with styling/layout also ux is kind of...I don't know...not good? :)
- add indexes on title and content, for larger dataset search might be slow
- UserFeeds table should have composite key <UserId,FeedId>, current structure allows multiple subscriptions for the same user
- variable 'first' taking too much events on it, spread trough the code, refactor to fire an event everytime first needs to be changed, subscribe on it
- in general news component.ts is kind of messy I like to refactor, but no time :(
