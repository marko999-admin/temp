﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
    public class Feed
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public int NumOfSubscriptions { get; set; }

        public bool Subscribed
        {
            get
            {
                return this.NumOfSubscriptions > 0 ? true : false;
            }
            
        }
    }
}
