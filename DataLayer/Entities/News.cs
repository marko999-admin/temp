﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Entities
{
    public class News
    {
        public string Title { get; set; }
        public string Content { get; set; }

    }
}
