﻿using DataLayer.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;

namespace DataLayer
{
    public class Provider
    {
        private readonly string _connectionString;

        public Provider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<Feed> GetFeeds(bool onlySubscribed = false)
        {
            IEnumerable<Feed> list;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = @"select f.Id, f.Title, 
	                        CASE 
		                        WHEN sub.NumOfSubs IS NOT NULL THEN sub.NumOfSubs
		                        ELSE 0
	                        END as NumOfSubscriptions
                           from Feeds f
                            left join (select uf.FeedId, uf.UserId, COUNT(uf.UserId) as NumOfSubs
		                    from UserFeeds uf
		                    group by uf.FeedId, uf.UserId
		                    having uf.UserId = 1) sub on f.Id = sub.FeedId";

                if (onlySubscribed)
                {
                    query += " join UserFeeds uf on f.Id = uf.FeedId where uf.UserId = 1";
                }
                
                list = connection.Query<Feed>(query);
                
            }
            return list;
        }

        public IEnumerable<News> GetNews()
        {
            IEnumerable<News> list;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query = "SELECT Title, Content FROM News";
                list = connection.Query<News>(query);
            }
            return list;
        }

        public int Unsubsribe(int feedId, int userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query = "DELETE FROM UserFeeds WHERE UserId=@userId AND FeedId=@feedId";
                return connection.Execute(query, new { userId = userId, feedId = feedId });
            }
        }

        public int Subscribe(int feedId, int userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query = "INSERT INTO UserFeeds (UserId, FeedId) VALUES (@userId, @feedId)";
                return connection.Execute(query, new { userId = userId, feedId = feedId });
            }
        }

        public IEnumerable<News> Search(int feedId, string search)
        {
            IEnumerable<News> list;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var searchExp = "%" + search + "%";
                string query = "SELECT Title,Content FROM News WHERE (Title LIKE @searchExp OR  Content LIKE @searchExp)";
                if(feedId > 0)
                {
                    query += " AND Feed = @feedId";
                }
                list = connection.Query<News>(query, new { searchExp = searchExp, feedId = feedId });
            }
            return list;
        }

        public IEnumerable<News> GetNewsFromFeed(int feedId)
        {
            IEnumerable<News> list;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query = "SELECT [News].Title, [News].Content FROM News WHERE [News].Feed = @feedId";
                list = connection.Query<News>(query, new { feedId = feedId });
            }
            return list;
        }
    }
}
