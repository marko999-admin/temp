import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  public dataSource: any;
  public displayedColumns = ['title', 'content']
  public feeds: any
  public searchValue = ''
  selectedFeed: any;
  onlySubscribed: boolean = false;
  isFeedSub: boolean = false;
  first: boolean = false;
  
  constructor(private dataService:DataService) { }

  ngOnInit(): void {

    this.dataService.getFeeds().subscribe(f=>{
      var allObj:any = {id: '0',title: 'All'};
      (f as Object[]).unshift(allObj)
      this.feeds = f;
      if(this.feeds.length > 0){
        //hm not sure why this is not triggering onFeedChange, it is two-way binded
        this.selectedFeed = this.feeds[0].id;
        this.first = true;
        var feed = this.feeds.find(f=>f.id == this.selectedFeed)
        this.isFeedSub = feed.subscribed;
      }
      this.dataService.getNewsForFeed(this.selectedFeed).subscribe(res=>{
        this.dataSource = res;
      })
    })
  }

  onFeedChange($event){
    this.selectedFeed = $event.value;
    if($event.value == 0){
      this.first = true;
    }else{
      this.first = false;
    }
    if(this.selectedFeed > 0){
      var feed = this.feeds.find(f=>f.id == this.selectedFeed)
      this.isFeedSub = feed.subscribed;
    }
    this.dataService.getNewsForFeed(this.selectedFeed).subscribe(res=>{
      this.dataSource = res;
    })
    this.searchValue = "";
  }

  search(){
    this.dataService.search(this.selectedFeed, this.searchValue).subscribe(res=>{
      this.dataSource = res;
    })
  }

  subscribe(){
    this.dataService.subscribe(this.selectedFeed, true).subscribe(t=>{
      if(t>0){
        this.isFeedSub = true;
        var feed = this.feeds.find(f=>f.id == this.selectedFeed);
        feed.subscribed = true;
        
      }
    })
  }

  unsubscribe(){
    this.dataService.subscribe(this.selectedFeed, false).subscribe(t=>{
      if(t>0){
        this.isFeedSub = false;
        var feed = this.feeds.find(f=>f.id == this.selectedFeed);
        feed.subscribed = false;
      }
    })
  }

  onlySubscribedChanged($event){
    //only if there is an action on checkbox
    if($event.checked != this.onlySubscribed){
      this.onlySubscribed = $event.checked;
      this.dataService.getFeeds(this.onlySubscribed).subscribe(res=>{
        if(!this.onlySubscribed){
          var allObj:any = {id: '0',title: 'All'};
          (res as Object[]).unshift(allObj)
        }
        this.feeds = res;
        if(this.feeds.length > 0){
          //hm not sure why this is not triggering onFeedChange, it is two-way binded
          this.selectedFeed = this.feeds[0].id;
        }
        this.dataService.getNewsForFeed(this.selectedFeed).subscribe(res=>{
          this.dataSource = res;
        })
      })
    }
  }
}
