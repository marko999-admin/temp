import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  api:string = "http://localhost:60194/api"
  
  constructor(private http: HttpClient) { }

  getNews(){
    return this.http.get(`${this.api}/news`);
  }

  getFeeds(onlySubscribed=false){
    var url = `${this.api}/feeds`;
    if(onlySubscribed){
      url+=`/${onlySubscribed}`
    }
    return this.http.get(url);
  }

  getNewsForFeed(id){
    return this.http.get(`${this.api}/news/${id}`);
  }

  search(feedId, searchTerm){
    return this.http.get(`${this.api}/search/${feedId}/${searchTerm}`)
  }

  subscribe(feedId, subscribe){
    let httpHeaders = new HttpHeaders({
      'CONTENT-TYPE' : 'application/json'
    }); 
    
    var payload = {
      feedId: feedId,
      subscribe: subscribe,
      userId: 1
    }
    
    var url = `${this.api}/feeds`
    return this.http.post(url, payload, {headers: httpHeaders})
  }
}