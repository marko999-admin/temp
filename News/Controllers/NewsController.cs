﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DataLayer.Entities;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace News.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class NewsController : Controller
    {
        private Provider _provider;

        public NewsController(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            _provider = new Provider(connectionString);
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<DataLayer.Entities.News> Get()
        {
            var news = _provider.GetNews();
            return news;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IEnumerable<DataLayer.Entities.News> Get(int id)
        {
            if(id == 0)
            {
                return _provider.GetNews();
            }
            else
            {
                return _provider.GetNewsFromFeed(id);
            }
        }
    }
}
