﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace News.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class FeedsController : Controller
    {
        private Provider _provider;

        public FeedsController(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            _provider = new Provider(connectionString);
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Object> Get()
        {
            var feeds = _provider.GetFeeds().Select(t => new { t.Id, t.Title, t.Subscribed });
            return feeds;
        }

        // GET api/<controller>/5
        [HttpGet("{onlySubscribed}")]
        public IEnumerable<Object> Get(bool onlySubscribed)
        {
            var feeds = _provider.GetFeeds(onlySubscribed).Select(t => new { t.Id, t.Title, t.Subscribed });
            return feeds; ;
        }

        // POST api/<controller>
        [HttpPost]
        public int Post([FromBody]SubsribeObject subObject)
        {
            if (subObject.subscribe) return _provider.Subscribe(subObject.feedId, subObject.userId);
            else return _provider.Unsubsribe(subObject.feedId, subObject.userId);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class SubsribeObject
    {
        public int feedId { get; set; }
        public bool subscribe { get; set; }
        public int userId { get; set; }
    }
}
