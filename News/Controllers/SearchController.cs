﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace News.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowOrigin")]
    public class SearchController : Controller
    {
        private Provider _provider;
    
        public SearchController(IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            _provider = new Provider(connectionString);
        }

        // GET api/<controller>/feedId/searchterm
        [HttpGet("{feedId}/{search}")]
        public IEnumerable<DataLayer.Entities.News> Get(int feedId, string search)
        {
            return _provider.Search(feedId, search);
        }

    }
}
